package com.softdesign.skillbrunchmainapp;

import android.app.Application;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * @author Sergey Vorobyev
 */

public class App extends Application {

    private static App sAppInstance;
    private static SharedPreferences sSharedPreferences;

    public static App get() {
        return sAppInstance;
    }

    public static SharedPreferences getSharedPreferences() {
        return sSharedPreferences;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sAppInstance = this;
        sSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
    }
}
