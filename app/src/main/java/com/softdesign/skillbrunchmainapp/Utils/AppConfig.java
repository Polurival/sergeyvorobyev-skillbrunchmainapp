package com.softdesign.skillbrunchmainapp.utils;

/**
 * @author Sergey Vorobyev.
 */
@SuppressWarnings("unused")
public interface AppConfig {

    int MAX_CONNECT_TIMEOUT = 1000 * 3;
    int MAX_READ_TIMEOUT = 1000 * 6;
}
