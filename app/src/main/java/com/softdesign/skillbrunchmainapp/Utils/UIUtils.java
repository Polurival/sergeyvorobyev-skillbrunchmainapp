package com.softdesign.skillbrunchmainapp.utils;

import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.softdesign.skillbrunchmainapp.App;
import com.softdesign.skillbrunchmainapp.R;

import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import retrofit2.adapter.rxjava.HttpException;

/**
 * @author Sergey Vorobyev
 */

@SuppressWarnings({"unused", "WeakerAccess"})
public class UIUtils {

    public static void showToast(Handler handler, String message) {
        if (handler != null) {
            handler.post(() -> showToast(message));
        } else {
            showToast(message);
        }
    }

    public static void showToast(String message) {
        Toast.makeText(App.get(), message, Toast.LENGTH_LONG).show();
    }

    public static void handleResponseError(Handler handler,
                                           String logTag, Throwable error) {
        if (error instanceof HttpException) {
            HttpException exception = (HttpException) error;
            if (exception.code() == 403) {
                Log.d(logTag, "handleError(): wrong username or password");
                showToast(handler, App.get().getString(R.string.error_wrong_username_or_password));
            } else {
                Log.d(logTag, "handleError(): response code is " + exception.code());
                showToast(handler, App.get().getString(R.string.error_unknown_error)
                        + " " + exception.code());
            }
        } else if (error instanceof SocketTimeoutException || error instanceof UnknownHostException) {
            Log.e(logTag, "onResponse(): check internet connection", error);
            showToast(handler, App.get().getString(R.string.error_failed_to_connect_to_server));
        } else if (error instanceof Exception) {
            Log.e(logTag, "onResponse(): unknown error", error);
            showToast(handler, App.get().getString(R.string.error_unknown_error));
        }
    }

    public static void measureView(View view) {
        int measureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        view.measure(measureSpec, measureSpec);
    }
}
