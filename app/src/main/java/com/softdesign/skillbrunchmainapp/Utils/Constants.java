package com.softdesign.skillbrunchmainapp.utils;

/**
 * @author Sergey Vorobyev
 */

@SuppressWarnings("unused")
public interface Constants {

    String LOG_TAG_PREFIX = "Skill";

    String PATTERN_EMAIL = "^[\\w\\+\\.\\%\\-]{3,}\\@[a-zA-Z0-9][a-zA-Z0-9\\-]{1,64}(\\.[a-zA-Z0-9][a-zA-Z0-9\\-]{1,25})+$";
}
