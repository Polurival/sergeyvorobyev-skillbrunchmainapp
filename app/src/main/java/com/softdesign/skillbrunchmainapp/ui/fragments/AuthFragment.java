package com.softdesign.skillbrunchmainapp.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.CycleInterpolator;

import com.softdesign.skillbrunchmainapp.R;
import com.softdesign.skillbrunchmainapp.data_binding.view_models.AbsViewModel;
import com.softdesign.skillbrunchmainapp.data_binding.view_models.AuthViewModel;
import com.softdesign.skillbrunchmainapp.databinding.FragmentAuthBinding;
import com.softdesign.skillbrunchmainapp.mvp.presenters.AuthPresenter;
import com.softdesign.skillbrunchmainapp.mvp.presenters.impl.AuthPresenterImpl;
import com.softdesign.skillbrunchmainapp.mvp.views.AuthView;
import com.softdesign.skillbrunchmainapp.utils.SimpleTextWatcher;

/**
 * @author Sergey Vorobyev.
 */

public class AuthFragment extends BaseFragment implements AuthView, View.OnClickListener {

    public static final String FRAGMENT_TAG = "FRAGMENT_TAG";

    private static final int ANIMATION_TRANSLATION = 10;
    private static final int ANIMATION_DURATION = 200;
    private static final int ANIMATION_CIRCLES = 5;

    private AuthPresenter mPresenter;
    private FragmentAuthBinding mBinding;
    private AuthViewModel mViewModel;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter = AuthPresenterImpl.getInstance();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        mBinding = FragmentAuthBinding.inflate(inflater, container, false);
        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
        mPresenter.onViewCreated(this);
    }

    @Override
    public void onDestroyView() {
        mPresenter.onDestroyView();
        super.onDestroyView();
    }

    @Override
    public void showCatalogButton(boolean isShow) {
        mViewModel.setShowingCatalogButton(isShow);
    }

    @Override
    public void showInputs(boolean isShow) {
        mViewModel.setShowingInputs(isShow);
    }

    @Override
    public void showProgress(boolean isShow) {
        mViewModel.setShowingProgress(isShow);
    }

    @Override
    public void showSocialButtons(boolean isShow) {
        mViewModel.setShowingSocialButtons(isShow);
    }

    @Override
    public void setEnabledLoginButton(boolean isEnable) {
        mViewModel.setLoginButtonEnabled(isEnable);
    }

    @Override
    public boolean isInputsShowed() {
        return mViewModel.isShowingInputs();
    }

    @Override
    public void setLoginBtnTextLogin() {
        mViewModel.setLoginButtonText(getString(R.string.auth_button_log_in));
    }

    @Override
    public void setLoginBtnTextLogout() {
        mViewModel.setLoginButtonText(getString(R.string.auth_button_log_out));
    }

    @Override
    public void setEmailValid(boolean isValid) {
        mViewModel.setEmailValid(isValid);
    }

    @Override
    public void setPasswordValid(boolean isValid) {
        mViewModel.setPasswordValid(isValid);
    }

    @Override
    public String getEmail() {
        return mViewModel.getEmail();
    }

    @Override
    public String getPassword() {
        return mViewModel.getPassword();
    }

    @Override
    public void bindViewModel(AbsViewModel viewModel) {
        mViewModel = (AuthViewModel) viewModel;
        mBinding.setAuthModel(mViewModel);
    }

    @Override
    public void showMessageCommon(String message) {
        showSnackBar(message);
    }

    @Override
    public void showMessageWrongEmailOrPassword() {
        String message = getString(R.string.error_wrong_username_or_password);
        showSnackBar(message);
    }

    @Override
    public void showMessageLoggedIn() {
        String message = getString(R.string.auth_snack_logged_in);
        showSnackBar(message);
    }

    @Override
    public void navigateToCatalog() {
        pressSystemBackButton();
    }

    @Override
    public void pressSystemBackButton() {
        super.onBackPressed();
    }

    @Override
    public void onBackPressed() {
        mPresenter.onBackPressed();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.login_btn:
                mPresenter.onLoginButtonClick();
                break;
            case R.id.show_catalog_btn:
                mPresenter.onShowCatalogButtonClick();
                break;
            case R.id.fb_btn:
                animateSocialBtn(mBinding.fbBtn);
                mPresenter.onFbButtonClick();
                break;
            case R.id.vk_btn:
                animateSocialBtn(mBinding.vkBtn);
                mPresenter.onVkButtonClick();
                break;
            case R.id.twitter_btn:
                animateSocialBtn(mBinding.twitterBtn);
                mPresenter.onTwitterButtonClick();
                break;
        }
    }

    private void showSnackBar(String message) {
        Snackbar.make(mBinding.coordinatorContainer, message, Snackbar.LENGTH_LONG).show();
    }

    private void init() {
        mBinding.fbBtn.setOnClickListener(this);
        mBinding.vkBtn.setOnClickListener(this);
        mBinding.twitterBtn.setOnClickListener(this);
        mBinding.loginBtn.setOnClickListener(this);
        mBinding.showCatalogBtn.setOnClickListener(this);
        mBinding.loginEmailEt.addTextChangedListener(new SimpleTextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                mPresenter.onEmailChanged();
            }
        });
        mBinding.loginPasswordEt.addTextChangedListener(new SimpleTextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                mPresenter.onPasswordChanged();
            }
        });
    }

    private void animateSocialBtn(View button) {
        button.animate()
                .translationX(ANIMATION_TRANSLATION)
                .setDuration(ANIMATION_DURATION)
                .setInterpolator(new CycleInterpolator(ANIMATION_CIRCLES));
    }
}
