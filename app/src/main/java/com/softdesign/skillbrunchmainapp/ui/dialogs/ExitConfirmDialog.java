package com.softdesign.skillbrunchmainapp.ui.dialogs;

import android.app.Dialog;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

import com.softdesign.skillbrunchmainapp.R;

/**
 * @author Sergey Vorobyev.
 */

public class ExitConfirmDialog extends DialogFragment {

    public static final String FRAGMENT_TAG = "FRAGMENT_TAG";
    public static final String KEY_DIALOG_MASSAGE = "ExitConfirmDialog_KEY_DIALOG_MASSAGE";

    private OnClickListener mOnPositiveButtonClickListener;
    private OnClickListener mOnNegativeButtonClickListener;

    public static ExitConfirmDialog newInstance(String message) {
        ExitConfirmDialog dialog = new ExitConfirmDialog();
        Bundle bundle = new Bundle();
        bundle.putString(KEY_DIALOG_MASSAGE, message);
        dialog.setRetainInstance(true);
        dialog.setArguments(bundle);
        return dialog;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        String message = getArguments().getString(KEY_DIALOG_MASSAGE);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.dialog_attention_title)
                .setMessage(message)
                .setPositiveButton(R.string.dialog_button_yes, mOnPositiveButtonClickListener)
                .setNegativeButton(R.string.dialog_button_no, mOnNegativeButtonClickListener);

        AlertDialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);
        return dialog;
    }

    @Override
    public void onDestroyView() {
        //https://code.google.com/p/android/issues/detail?id=17423
        if (getDialog() != null && getRetainInstance()) {
            getDialog().setDismissMessage(null);
        }
        super.onDestroyView();
    }

    public void setOnPositiveButtonClickListener(OnClickListener onPositiveButtonClickListener) {
        mOnPositiveButtonClickListener = onPositiveButtonClickListener;
    }

    public void setOnNegativeButtonClickListener(OnClickListener onNegativeButtonClickListener) {
        mOnNegativeButtonClickListener = onNegativeButtonClickListener;
    }
}
