package com.softdesign.skillbrunchmainapp.ui.activities;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView.OnNavigationItemSelectedListener;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.MenuItem;

import com.softdesign.skillbrunchmainapp.R;
import com.softdesign.skillbrunchmainapp.data_binding.view_models.AbsViewModel;
import com.softdesign.skillbrunchmainapp.data_binding.view_models.MainViewModel;
import com.softdesign.skillbrunchmainapp.databinding.ActivityMainBinding;
import com.softdesign.skillbrunchmainapp.mvp.presenters.MainPresenter;
import com.softdesign.skillbrunchmainapp.mvp.presenters.impl.MainPresenterImpl;
import com.softdesign.skillbrunchmainapp.mvp.views.MainView;
import com.softdesign.skillbrunchmainapp.ui.dialogs.ExitConfirmDialog;
import com.softdesign.skillbrunchmainapp.ui.fragments.AccountFragment;
import com.softdesign.skillbrunchmainapp.ui.fragments.AuthFragment;
import com.softdesign.skillbrunchmainapp.ui.fragments.CatalogFragment;

public class MainActivity extends BaseActivity implements MainView, OnNavigationItemSelectedListener {

    private MainPresenter mPresenter;
    private ActivityMainBinding mBinding;
    private MainViewModel mViewModel;
    private FragmentManager mFragmentManager;
    private ExitConfirmDialog mExitConfirmDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.AppTheme);
        mPresenter = MainPresenterImpl.getInstance();
        mFragmentManager = getSupportFragmentManager();
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);

    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mPresenter.onViewCreated(this);
        initToolbar();
        initDrawer(savedInstanceState == null);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            mPresenter.onDrawerToggleClick();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        mPresenter.onDestroyView();
        super.onDestroy();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        mBinding.navView.setCheckedItem(item.getItemId());
        switch (id) {
            case R.id.nav_account:
                mPresenter.onNavigationItemAccountClick();
                break;
            case R.id.nav_catalog:
                mPresenter.onNavigationItemCatalogClick();
                break;
            case R.id.nav_favorites:
                mPresenter.onNavigationItemFavoritesClick();
                break;
            case R.id.nav_orders:
                mPresenter.onNavigationItemOrdersClick();
                break;
            case R.id.nav_notifications:
                mPresenter.onNavigationItemNotificationsClick();
                break;
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        mPresenter.onSystemBackButtonClick();
    }

    @Override
    public void pressSystemBackButton() {
        super.onBackPressed();
    }

    @Override
    public void setAvatar(String uri) {
        mViewModel.setDrawerHeaderAvatar(uri);
    }

    @Override
    public void setUserName(String username) {
        mViewModel.setDrawerHeaderUsername(username);
    }

    @Override
    public void navigateToAccount() {
        AccountFragment f = (AccountFragment) mFragmentManager
                .findFragmentByTag(AccountFragment.FRAGMENT_TAG);
        if (f == null) {
            f = new AccountFragment();
        }
        mFragmentManager.beginTransaction()
                .replace(R.id.fragment_container, f, AccountFragment.FRAGMENT_TAG)
                .commit();
    }

    @Override
    public void navigateToCatalog() {
        AuthFragment af = (AuthFragment) mFragmentManager.findFragmentByTag(AuthFragment.FRAGMENT_TAG);
        if (af != null && af.isResumed()) {
            mFragmentManager.popBackStack();
        } else {
            CatalogFragment cf = (CatalogFragment) mFragmentManager
                    .findFragmentByTag(CatalogFragment.FRAGMENT_TAG);
            if (cf == null) cf = new CatalogFragment();
            mFragmentManager.beginTransaction()
                    .replace(R.id.fragment_container, cf, CatalogFragment.FRAGMENT_TAG)
                    .commit();
        }
    }

    @Override
    public void navigateToFavorites() {

    }

    @Override
    public void navigateToOrders() {

    }

    @Override
    public void navigateToNotifications() {

    }

    @Override
    public void openDrawer() {
        mBinding.drawer.openDrawer(GravityCompat.START);
    }

    @Override
    public void closeDrawer() {
        mBinding.drawer.closeDrawer(GravityCompat.START);
    }

    @Override
    public void showExitConfirmDialog() {
        if (mExitConfirmDialog == null)
            initExitConfirmDialog();
        mExitConfirmDialog.show(getSupportFragmentManager(), ExitConfirmDialog.FRAGMENT_TAG);
    }

    @Override
    public void exit() {
        finish();
    }

    @Override
    public boolean isDrawerOpened() {
        return mBinding.drawer.isDrawerOpen(GravityCompat.START);
    }

    @Override
    public boolean isBackStackIsEmpty() {
        return getSupportFragmentManager().getBackStackEntryCount() == 0;
    }

    @Override
    public void bindViewModel(AbsViewModel viewModel) {
        mViewModel = (MainViewModel) viewModel;
        mBinding.setMainModel(mViewModel);
    }

    @Override
    public void showMessageCommon(String message) {
        showSnackBar(message);
    }

    private void initDrawer(boolean isFirstCreate) {
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, mBinding.drawer,
                mBinding.toolbar, R.string.drawer_open, R.string.drawer_close);
        mBinding.drawer.addDrawerListener(toggle);
        toggle.syncState();
        mBinding.navView.setNavigationItemSelectedListener(this);
        if (isFirstCreate) onNavigationItemSelected(mBinding.navView.getMenu().getItem(1));
    }

    private void initToolbar() {
        setSupportActionBar(mBinding.toolbar);
    }

    private void showSnackBar(String message) {
        Snackbar.make(mBinding.coordinatorContainer, message, Snackbar.LENGTH_LONG).show();
    }

    private void initExitConfirmDialog() {
        mExitConfirmDialog = ExitConfirmDialog.newInstance(getString(R.string.catalog_dialog_exit_confirm_mesaage));
        mExitConfirmDialog.setOnPositiveButtonClickListener((d, w) -> mPresenter.onPositiveDialogButtonClick());
        mExitConfirmDialog.setOnNegativeButtonClickListener((d, w) -> mPresenter.onNegativeDialogButtonClick());
    }
}
