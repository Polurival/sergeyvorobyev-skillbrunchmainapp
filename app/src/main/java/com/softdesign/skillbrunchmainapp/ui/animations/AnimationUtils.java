package com.softdesign.skillbrunchmainapp.ui.animations;

import android.animation.Animator;
import android.view.View;

/**
 * @author Sergey Vorobyev.
 */

public class AnimationUtils {

    private static final int ANIMATE_DURATION = 300;

    public static void animateAlphaVisibleGone(View view, boolean isShow) {
        view.animate()
                .setDuration(ANIMATE_DURATION)
                .alpha(isShow ? 1 : 0)
                .setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {
                        if (isShow) {
                            view.setVisibility(View.VISIBLE);
                        } else {
                            view.setAlpha(1);
                        }
                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        if (!isShow) {
                            view.setVisibility(View.GONE);
                        }
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {
                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {
                    }
                });
    }

    public static void animateAlphaVisibleInvisible(View view, boolean isShow) {
        view.animate()
                .setDuration(ANIMATE_DURATION)
                .alpha(isShow ? 1 : 0)
                .setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {
                        if (isShow) {
                            view.setVisibility(View.VISIBLE);
                        } else {
                            view.setAlpha(1);
                        }
                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        if (!isShow) {
                            view.setVisibility(View.INVISIBLE);
                        }
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {
                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {
                    }
                });
    }
}
