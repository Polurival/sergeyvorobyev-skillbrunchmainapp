package com.softdesign.skillbrunchmainapp.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.MenuItemCompat;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.softdesign.skillbrunchmainapp.R;
import com.softdesign.skillbrunchmainapp.data.storage.dto.ProductDto;
import com.softdesign.skillbrunchmainapp.data_binding.view_models.AbsViewModel;
import com.softdesign.skillbrunchmainapp.data_binding.view_models.CatalogViewModel;
import com.softdesign.skillbrunchmainapp.databinding.FragmentCatalogBinding;
import com.softdesign.skillbrunchmainapp.mvp.presenters.CatalogPresenter;
import com.softdesign.skillbrunchmainapp.mvp.presenters.impl.CatalogPresenterImpl;
import com.softdesign.skillbrunchmainapp.mvp.views.CatalogView;
import com.softdesign.skillbrunchmainapp.ui.activities.MainActivity;
import com.softdesign.skillbrunchmainapp.ui.adapters.CatalogPagerAdapter;

import java.util.List;

/**
 * @author Sergey Vorobyev.
 */

public class CatalogFragment extends BaseFragment implements CatalogView {

    public static final String FRAGMENT_TAG = "CatalogFragment";

    private CatalogPresenter mPresenter;
    private FragmentCatalogBinding mBinding;
    private FragmentManager mFragmentManager;

    private TextView mToolbarCount;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        mPresenter = CatalogPresenterImpl.getInstance();
        mFragmentManager = getActivity().getSupportFragmentManager();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        mBinding = FragmentCatalogBinding.inflate(inflater, container, false);
        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mBinding.addToCardBtn.setOnClickListener(v ->
                mPresenter.OnBuyButtonClick(mBinding.catalogPager.getCurrentItem()));
    }

    @Override
    public void onDestroyView() {
        mPresenter.onDestroyView();
        super.onDestroyView();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_toolbar, menu);
        MenuItem item = menu.findItem(R.id.cart);
        MenuItemCompat.setActionView(item, R.layout.toolbar_menu_item_cart);
        FrameLayout itemLayout = (FrameLayout) MenuItemCompat.getActionView(item);
        mToolbarCount = (TextView) itemLayout.findViewById(R.id.count);
        mPresenter.onViewCreated(this);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void showCatalog(List<ProductDto> productList) {
        CatalogPagerAdapter adapter = new CatalogPagerAdapter(getChildFragmentManager());
        for (ProductDto product : productList) adapter.addItem(product);
        mBinding.catalogPager.setAdapter(adapter);
        mBinding.indicator.setViewPager(mBinding.catalogPager);
    }

    @Override
    public void navigateToAuth() {
        AuthFragment f = (AuthFragment) mFragmentManager.findFragmentByTag(AuthFragment.FRAGMENT_TAG);
        if (f == null) f = new AuthFragment();
        mFragmentManager.beginTransaction()
                .replace(R.id.fragment_container, f, AuthFragment.FRAGMENT_TAG)
                .addToBackStack("AuthFragment")
                .commit();
    }

    @Override
    public void updateProductCounter(String counter) {
        mToolbarCount.setText(counter);
    }

    @Override
    public void showMessageAddToCard(String productName) {
        String prefix = getString(R.string.catalog_snack_added_to_cart_prefix);
        String postfix = getString(R.string.catalog_snack_added_to_cart_postfix);
        getMainActivity().showMessageCommon(prefix + " " + productName + " " + postfix);
    }

    @Override
    public void bindViewModel(AbsViewModel viewModel) {
        mBinding.setCatalogModel((CatalogViewModel) viewModel);
    }

    @Override
    public void showMessageCommon(String message) {
    }

    @Override
    public void pressSystemBackButton() {
    }

    private MainActivity getMainActivity() {
        return (MainActivity) getActivity();
    }
}
