package com.softdesign.skillbrunchmainapp.ui.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.softdesign.skillbrunchmainapp.data.storage.dto.ProductDto;
import com.softdesign.skillbrunchmainapp.ui.fragments.ProductFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Sergey Vorobyev.
 */

public class CatalogPagerAdapter extends FragmentPagerAdapter {

    private List<ProductDto> mProductList;

    public CatalogPagerAdapter(FragmentManager fm) {
        super(fm);
        mProductList = new ArrayList<>();
    }

    @Override
    public Fragment getItem(int position) {
        return ProductFragment.newInstance(mProductList.get(position));
    }

    @Override
    public int getCount() {
        return mProductList.size();
    }

    public void addItem(ProductDto product) {
        mProductList.add(product);
        notifyDataSetChanged();
    }
}
