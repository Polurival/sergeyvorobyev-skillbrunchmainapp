package com.softdesign.skillbrunchmainapp.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.softdesign.skillbrunchmainapp.R;
import com.softdesign.skillbrunchmainapp.data.storage.dto.ProductDto;
import com.softdesign.skillbrunchmainapp.data_binding.view_models.AbsViewModel;
import com.softdesign.skillbrunchmainapp.data_binding.view_models.ProductViewModel;
import com.softdesign.skillbrunchmainapp.databinding.FragmentProductBinding;
import com.softdesign.skillbrunchmainapp.mvp.presenters.ProductPresenter;
import com.softdesign.skillbrunchmainapp.mvp.presenters.ProductPresenterFactory;
import com.softdesign.skillbrunchmainapp.mvp.views.ProductView;
import com.softdesign.skillbrunchmainapp.ui.activities.MainActivity;

/**
 * @author Sergey Vorobyev.
 */

public class ProductFragment extends BaseFragment implements ProductView, View.OnClickListener {

    private static final String KEY_PRODUCT = "KEY_PRODUCT";

    private ProductPresenter mPresenter;
    private FragmentProductBinding mBinding;
    private ProductViewModel mViewModel;

    public ProductFragment() {
    }

    public static ProductFragment newInstance(ProductDto product) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(KEY_PRODUCT, product);
        ProductFragment fragment = new ProductFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        readBundle(getArguments());
        mBinding = FragmentProductBinding.inflate(inflater, container, false);
        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
        mPresenter.onViewCreated(this);
    }

    @Override
    public void onDestroyView() {
        mPresenter.onDestroyView();
        super.onDestroyView();
    }

    @Override
    public void showProduct(ProductDto product) {
        mViewModel.setName(product.getProductName());
        mViewModel.setDescription(product.getDescription());
        mViewModel.setImage(product.getImageUrl());
    }

    @Override
    public void updateCount(String count) {
        mViewModel.setCount(count);
    }

    @Override
    public void updatePrice(String price) {
        mViewModel.setPrice(price);
    }

    @Override
    public void bindViewModel(AbsViewModel viewModel) {
        mViewModel = (ProductViewModel) viewModel;
        mBinding.setProductModel(mViewModel);
    }

    @Override
    public void showMessageCommon(String message) {
        getMainActivity().showMessageCommon(message);
    }

    @Override
    public void pressSystemBackButton() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.minusBtn:
                mPresenter.onMinusButtonClick();
                break;
            case R.id.plusBtn:
                mPresenter.onPlusButtonClick();
                break;
        }
    }

    private void init() {
        mBinding.plusBtn.setOnClickListener(this);
        mBinding.minusBtn.setOnClickListener(this);
    }

    private void readBundle(Bundle bundle) {
        if (bundle != null) {
            ProductDto product = bundle.getParcelable(KEY_PRODUCT);
            mPresenter = ProductPresenterFactory.getInstance(product);
        }
    }

    private MainActivity getMainActivity() {
        return (MainActivity) getActivity();
    }
}
