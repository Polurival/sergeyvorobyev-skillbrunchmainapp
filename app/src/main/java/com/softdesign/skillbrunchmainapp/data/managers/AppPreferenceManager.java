package com.softdesign.skillbrunchmainapp.data.managers;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.softdesign.skillbrunchmainapp.App;

/**
 * @author Sergey Vorobyev
 */

public class AppPreferenceManager {

    private SharedPreferences mPreferences;

    public AppPreferenceManager() {
        mPreferences = App.getSharedPreferences();
    }

    public void saveString(String key, String token) {
        Editor editor = mPreferences.edit();
        editor.putString(key, token);
        editor.apply();
    }

    public void deleteString(String key) {
        Editor editor = mPreferences.edit();
        editor.remove(key);
        editor.apply();
    }

    public String getString(String key) {
        return mPreferences.getString(key, "");
    }
}
