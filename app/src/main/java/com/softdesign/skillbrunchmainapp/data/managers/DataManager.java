package com.softdesign.skillbrunchmainapp.data.managers;

import com.softdesign.skillbrunchmainapp.data.network.PicassoCache;
import com.softdesign.skillbrunchmainapp.data.storage.dto.ProductDto;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Sergey Vorobyev.
 */

public class DataManager {

    private static final String PREF_KEY_AUTH_TOKEN = "PREF_KEY_AUTH_TOKEN";

    private static final DataManager ourInstance = new DataManager();

    private AppPreferenceManager mPreferenceManager;
    private Picasso mPicasso;
    private List<ProductDto> mMockProductList;

    private DataManager() {
        mPreferenceManager = new AppPreferenceManager();
        mPicasso = new PicassoCache().getPicassoInstance();
        generateMockData();
    }

    public static DataManager getInstance() {
        return ourInstance;
    }

    public Picasso getPicasso() {
        return mPicasso;
    }

    public boolean checkAuthorisation() {
        return !mPreferenceManager.getString(PREF_KEY_AUTH_TOKEN).isEmpty();
    }

    public void saveToken(String token) {
        mPreferenceManager.saveString(PREF_KEY_AUTH_TOKEN, token);
    }

    public void deleteToken() {
        mPreferenceManager.deleteString(PREF_KEY_AUTH_TOKEN);
    }

    public ProductDto getProductById(long id) {
        // TODO: 31.10.2016 this temp simple mock data fix me (may be load from db)
        return mMockProductList.get((int) (id - 1));
    }

    public void updateProduct(ProductDto product) {
        // TODO: 31.10.2016 update product count or status (something in product) save in DB 
    }

    public List<ProductDto> getProductList() {
        // TODO: 31.10.2016  load product list from anywhere
        return mMockProductList;
    }

    private void generateMockData() {
        mMockProductList = new ArrayList<>();
        mMockProductList.add(new ProductDto(1, "test1", "file:///android_asset/radio_image.jpg", "description description description description description description", 100, 1));
        mMockProductList.add(new ProductDto(2, "test2", "file:///android_asset/radio_image.jpg", "description description description description description description", 150, 2));
        mMockProductList.add(new ProductDto(3, "test3", "file:///android_asset/radio_image.jpg", "description description description description description description", 200, 3));
        mMockProductList.add(new ProductDto(4, "test4", "file:///android_asset/radio_image.jpg", "description description description description description description", 250, 4));
        mMockProductList.add(new ProductDto(5, "test5", "file:///android_asset/radio_image.jpg", "description description description description description description", 300, 5));
        mMockProductList.add(new ProductDto(6, "test6", "file:///android_asset/radio_image.jpg", "description description description description description description", 350, 6));
        mMockProductList.add(new ProductDto(7, "test3", "file:///android_asset/radio_image.jpg", "description description description description description description", 200, 3));
        mMockProductList.add(new ProductDto(8, "test4", "file:///android_asset/radio_image.jpg", "description description description description description description", 250, 4));
        mMockProductList.add(new ProductDto(9, "test5", "file:///android_asset/radio_image.jpg", "description description description description description description", 300, 5));
        mMockProductList.add(new ProductDto(10, "test6", "file:///android_asset/radio_image.jpg", "description description description description description description", 350, 6));
    }
}
