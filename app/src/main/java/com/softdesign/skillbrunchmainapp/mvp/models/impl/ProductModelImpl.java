package com.softdesign.skillbrunchmainapp.mvp.models.impl;

import com.softdesign.skillbrunchmainapp.data.managers.DataManager;
import com.softdesign.skillbrunchmainapp.data.storage.dto.ProductDto;
import com.softdesign.skillbrunchmainapp.mvp.models.ProductModel;

/**
 * @author Sergey Vorobyev.
 */

public class ProductModelImpl implements ProductModel {

    private DataManager mDataManager = DataManager.getInstance();

    public ProductDto getProductById(long id) {
        return mDataManager.getProductById(id);
    }

    @Override
    public void updateProduct(ProductDto product) {
        mDataManager.updateProduct(product);
    }
}
