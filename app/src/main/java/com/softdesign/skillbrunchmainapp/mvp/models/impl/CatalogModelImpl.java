package com.softdesign.skillbrunchmainapp.mvp.models.impl;

import com.softdesign.skillbrunchmainapp.data.managers.DataManager;
import com.softdesign.skillbrunchmainapp.data.storage.dto.ProductDto;
import com.softdesign.skillbrunchmainapp.mvp.models.CatalogModel;

import java.util.List;

/**
 * @author Sergey Vorobyev.
 */

public class CatalogModelImpl implements CatalogModel {

    private DataManager mDataManager;

    public CatalogModelImpl() {
        mDataManager = DataManager.getInstance();
    }

    @Override
    public List<ProductDto> getProductList() {
        return mDataManager.getProductList();
    }

    @Override
    public ProductDto getProductById(long id) {
        return mDataManager.getProductById(id);
    }

    @Override
    public boolean checkAuthorisation() {
        return mDataManager.checkAuthorisation();
    }
}
