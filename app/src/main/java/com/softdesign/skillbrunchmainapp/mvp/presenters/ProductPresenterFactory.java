package com.softdesign.skillbrunchmainapp.mvp.presenters;

import android.support.v4.util.LongSparseArray;

import com.softdesign.skillbrunchmainapp.data.storage.dto.ProductDto;
import com.softdesign.skillbrunchmainapp.mvp.presenters.impl.ProductPresenterImpl;

/**
 * @author Sergey Vorobyev.
 */

public class ProductPresenterFactory {
    private static final LongSparseArray<ProductPresenter> sPresenterMap = new LongSparseArray<>();

    private static void registerPresenter(ProductDto product, ProductPresenter presenter) {
        sPresenterMap.put(product.getId(), presenter);
    }

    public static ProductPresenter getInstance(ProductDto product) {
        ProductPresenter presenter = sPresenterMap.get(product.getId());
        if (presenter == null) {
            presenter = ProductPresenterImpl.newInstance(product);
            registerPresenter(product, presenter);
        }
        return presenter;
    }
}
