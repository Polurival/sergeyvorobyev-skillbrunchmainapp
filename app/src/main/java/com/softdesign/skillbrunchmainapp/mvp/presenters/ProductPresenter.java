package com.softdesign.skillbrunchmainapp.mvp.presenters;

/**
 * @author Sergey Vorobyev.
 */

public interface ProductPresenter extends Presenter {

    void onPlusButtonClick();

    void onMinusButtonClick();
}
