package com.softdesign.skillbrunchmainapp.mvp.views;

/**
 * @author Sergey Vorobyev.
 */

public interface MainView extends MvpView {

    void setAvatar(String uri);

    void setUserName(String username);

    void navigateToAccount();

    void navigateToCatalog();

    void navigateToFavorites();

    void navigateToOrders();

    void navigateToNotifications();

    void openDrawer();

    void closeDrawer();

    void showExitConfirmDialog();

    void exit();

    boolean isDrawerOpened();

    boolean isBackStackIsEmpty();
}
