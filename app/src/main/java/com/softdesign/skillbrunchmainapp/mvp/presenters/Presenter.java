package com.softdesign.skillbrunchmainapp.mvp.presenters;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.softdesign.skillbrunchmainapp.mvp.views.MvpView;

/**
 * @author Sergey Vorobyev.
 */

public interface Presenter {

    @Nullable
    MvpView getView();

    void onViewCreated(@NonNull MvpView view);

    void onDestroyView();
}
