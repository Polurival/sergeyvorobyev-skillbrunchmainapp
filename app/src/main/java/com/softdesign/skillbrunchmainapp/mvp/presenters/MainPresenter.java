package com.softdesign.skillbrunchmainapp.mvp.presenters;

/**
 * @author Sergey Vorobyev.
 */

public interface MainPresenter extends Presenter {

    void onNavigationItemAccountClick();

    void onNavigationItemCatalogClick();

    void onNavigationItemFavoritesClick();

    void onNavigationItemOrdersClick();

    void onNavigationItemNotificationsClick();

    void onDrawerToggleClick();

    void onSystemBackButtonClick();

    void onPositiveDialogButtonClick();

    void onNegativeDialogButtonClick();
}
