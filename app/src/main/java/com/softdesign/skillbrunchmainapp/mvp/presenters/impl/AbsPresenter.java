package com.softdesign.skillbrunchmainapp.mvp.presenters.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.softdesign.skillbrunchmainapp.data_binding.view_models.AbsViewModel;
import com.softdesign.skillbrunchmainapp.mvp.presenters.Presenter;
import com.softdesign.skillbrunchmainapp.mvp.views.MvpView;

/**
 * @author Sergey Vorobyev.
 */

abstract class AbsPresenter<V extends MvpView, VM extends AbsViewModel>
        implements Presenter {

    private V mView;
    private VM mViewModel;
    private boolean mIsFirstStart = true;

    @Override
    @SuppressWarnings("unchecked")
    public void onViewCreated(@NonNull MvpView view) {
        mView = (V) view;

        if (mViewModel == null) mViewModel = createViewModel();
        mView.bindViewModel(mViewModel);

        if (mIsFirstStart) {
            mIsFirstStart = false;
            onFirstViewCreated();
        }
        onViewCreated();
    }

    @Override
    public void onDestroyView() {
        mView = null;
    }

    @Nullable
    @Override
    public V getView() {
        return mView;
    }

    protected abstract VM createViewModel();

    protected abstract void onViewCreated();

    protected abstract void onFirstViewCreated();
}
