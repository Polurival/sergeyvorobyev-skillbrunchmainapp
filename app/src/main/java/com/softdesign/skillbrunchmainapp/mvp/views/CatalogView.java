package com.softdesign.skillbrunchmainapp.mvp.views;

import com.softdesign.skillbrunchmainapp.data.storage.dto.ProductDto;

import java.util.List;

/**
 * @author Sergey Vorobyev.
 */

public interface CatalogView extends MvpView {

    void showCatalog(List<ProductDto> productList);

    void navigateToAuth();

    void updateProductCounter(String counter);

    void showMessageAddToCard(String productName);
}
