package com.softdesign.skillbrunchmainapp.mvp.presenters.impl;

import android.text.TextUtils;

import com.softdesign.skillbrunchmainapp.data_binding.view_models.AuthViewModel;
import com.softdesign.skillbrunchmainapp.mvp.models.AuthModel;
import com.softdesign.skillbrunchmainapp.mvp.models.impl.AuthModelImpl;
import com.softdesign.skillbrunchmainapp.mvp.presenters.AuthPresenter;
import com.softdesign.skillbrunchmainapp.mvp.views.AuthView;

import static com.softdesign.skillbrunchmainapp.utils.Constants.PATTERN_EMAIL;

/**
 * @author Sergey Vorobyev.
 */

public class AuthPresenterImpl extends AbsPresenter<AuthView, AuthViewModel>
        implements AuthPresenter {

    private static AuthPresenter instance = new AuthPresenterImpl();
    private boolean mIsLoading = false;
    private AuthModel mModel;

    private AuthPresenterImpl() {
        mModel = new AuthModelImpl();
    }

    public static AuthPresenter getInstance() {
        return instance;
    }

    @Override
    public void onEmailChanged() {
        if (getView() != null) {
            String email = getView().getEmail();
            String password = getView().getPassword();
            if (isValidEmail(email)) {
                getView().setEmailValid(true);
                if (isValidPassword(password) && !mIsLoading && getView().isInputsShowed()) {
                    getView().setEnabledLoginButton(true);
                }
            } else {
                getView().setEmailValid(false);
                if (getView().isInputsShowed()) getView().setEnabledLoginButton(false);
            }
        }
    }

    @Override
    public void onPasswordChanged() {
        if (getView() != null) {
            String email = getView().getEmail();
            String password = getView().getPassword();
            if (isValidPassword(password)) {
                getView().setPasswordValid(true);
                if (isValidEmail(email) && !mIsLoading && getView().isInputsShowed()) {
                    getView().setEnabledLoginButton(true);
                }
            } else {
                getView().setPasswordValid(false);
                if (getView().isInputsShowed()) getView().setEnabledLoginButton(false);
            }
        }
    }

    @Override
    public void onLoginButtonClick() {
        if (getView() != null) {
            if (mModel.checkAuthorisation()) {
                mModel.logout();
                getView().setLoginBtnTextLogin();
            } else {
                if (!getView().isInputsShowed()) {
                    getView().showInputs(true);
                    getView().showCatalogButton(false);
                    getView().showSocialButtons(true);
                    onEmailChanged();
                    onPasswordChanged();
                } else {
                    mIsLoading = true;
                    getView().showProgress(true);
                    getView().showInputs(false);
                    getView().showSocialButtons(false);
                    getView().showCatalogButton(false);
                    getView().setEnabledLoginButton(false);
                    mModel.login(getView().getEmail(), getView().getPassword())
                            .subscribe(isSuccess -> {
                                if (isSuccess) {
                                    onSuccess("token");
                                } else {
                                    onError();
                                }
                            });
                }
            }
        }
    }

    @Override
    public void onShowCatalogButtonClick() {
        if (getView() != null) {
            getView().navigateToCatalog();
        }
    }

    @Override
    public void onFbButtonClick() {
        if (getView() != null) {
            getView().showMessageCommon("Auth by facebook");
        }
    }

    @Override
    public void onVkButtonClick() {
        if (getView() != null) {
            getView().showMessageCommon("Auth by vk");
        }
    }

    @Override
    public void onTwitterButtonClick() {
        if (getView() != null) {
            getView().showMessageCommon("Auth by twitter");
        }
    }

    @Override
    public void onBackPressed() {
        if (getView() != null) {
            if (getView().isInputsShowed()) {
                getView().showCatalogButton(true);
                getView().showInputs(false);
                getView().setEnabledLoginButton(true);
                getView().showSocialButtons(false);
            } else {
                getView().pressSystemBackButton();
            }
        }
    }

    private void onSuccess(String token) {
        mIsLoading = false;
        if (getView() != null) {
            getView().showProgress(false);
            getView().showCatalogButton(true);
            getView().setEnabledLoginButton(true);
            getView().setLoginBtnTextLogout();
            mModel.saveToken(token);
            if (getView() != null) {
                getView().showMessageLoggedIn();
            }
        }
    }

    private void onError() {
        mIsLoading = false;
        if (getView() != null) {
            getView().showProgress(false);
            getView().showInputs(true);
            getView().setEnabledLoginButton(true);
            getView().showSocialButtons(true);
            getView().showMessageWrongEmailOrPassword();
        }
    }

    private boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && email.matches(PATTERN_EMAIL);
    }

    private boolean isValidPassword(String password) {
        return password != null && password.length() > 7;
    }

    @Override
    protected AuthViewModel createViewModel() {
        return new AuthViewModel();
    }

    @Override
    protected void onViewCreated() {

    }

    @Override
    protected void onFirstViewCreated() {
        if (getView() != null) {
            getView().showCatalogButton(true);
            getView().setEnabledLoginButton(true);
            if (mModel.checkAuthorisation()) {
                getView().setLoginBtnTextLogout();
            } else {
                getView().setLoginBtnTextLogin();
            }
        }
    }
}
