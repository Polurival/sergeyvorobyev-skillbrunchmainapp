package com.softdesign.skillbrunchmainapp.mvp.views;

import com.softdesign.skillbrunchmainapp.data.storage.dto.ProductDto;

/**
 * @author Sergey Vorobyev.
 */

public interface ProductView extends MvpView {

    void showProduct(ProductDto product);

    void updateCount(String count);

    void updatePrice(String price);
}
