package com.softdesign.skillbrunchmainapp.mvp.models;

import rx.Observable;

/**
 * @author Sergey Vorobyev.
 */

public interface AuthModel extends Model {

    boolean checkAuthorisation();

    Observable<Boolean> login(String email, String password);

    void logout();

    void saveToken(String token);
}
