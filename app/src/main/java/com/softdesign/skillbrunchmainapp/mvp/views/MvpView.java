package com.softdesign.skillbrunchmainapp.mvp.views;

import com.softdesign.skillbrunchmainapp.data_binding.view_models.AbsViewModel;

/**
 * @author Sergey Vorobyev.
 */

public interface MvpView {

    void bindViewModel(AbsViewModel viewModel);

    void showMessageCommon(String message);

    void pressSystemBackButton();
}
