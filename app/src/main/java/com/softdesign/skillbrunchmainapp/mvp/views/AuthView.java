package com.softdesign.skillbrunchmainapp.mvp.views;

/**
 * @author Sergey Vorobyev.
 */

public interface AuthView extends MvpView {

    void showCatalogButton(boolean isShow);

    void showInputs(boolean isShow);

    void showProgress(boolean isShow);

    void showSocialButtons(boolean isShow);

    void setEnabledLoginButton(boolean isEnable);

    boolean isInputsShowed();

    void setLoginBtnTextLogin();

    void setLoginBtnTextLogout();

    void setEmailValid(boolean isValid);

    void setPasswordValid(boolean isValid);

    String getEmail();

    String getPassword();

    void showMessageWrongEmailOrPassword();

    void showMessageLoggedIn();

    void navigateToCatalog();
}
