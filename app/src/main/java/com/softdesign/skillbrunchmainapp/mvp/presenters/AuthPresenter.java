package com.softdesign.skillbrunchmainapp.mvp.presenters;

/**
 * @author Sergey Vorobyev.
 */

public interface AuthPresenter extends Presenter {

    void onEmailChanged();

    void onPasswordChanged();

    void onLoginButtonClick();

    void onShowCatalogButtonClick();

    void onFbButtonClick();

    void onVkButtonClick();

    void onTwitterButtonClick();

    void onBackPressed();
}
