package com.softdesign.skillbrunchmainapp.mvp.presenters.impl;

import com.softdesign.skillbrunchmainapp.data.storage.dto.ProductDto;
import com.softdesign.skillbrunchmainapp.data_binding.view_models.ProductViewModel;
import com.softdesign.skillbrunchmainapp.mvp.models.ProductModel;
import com.softdesign.skillbrunchmainapp.mvp.models.impl.ProductModelImpl;
import com.softdesign.skillbrunchmainapp.mvp.presenters.ProductPresenter;
import com.softdesign.skillbrunchmainapp.mvp.views.ProductView;

/**
 * @author Sergey Vorobyev.
 */
public class ProductPresenterImpl extends AbsPresenter<ProductView, ProductViewModel>
        implements ProductPresenter {

    private ProductModel mModel;
    private ProductDto mData;

    private ProductPresenterImpl(ProductDto productDto) {
        mModel = new ProductModelImpl();
        mData = productDto;
    }

    public static ProductPresenter newInstance(ProductDto product) {
        return new ProductPresenterImpl(product);
    }

    @Override
    public void onPlusButtonClick() {
        mData.incrementCount();
        mModel.updateProduct(mData);
        updateProduct();
    }

    @Override
    public void onMinusButtonClick() {
        if (mData.getCount() > 0) {
            mData.decrementCount();
            updateProduct();
        }
    }

    @Override
    protected ProductViewModel createViewModel() {
        return new ProductViewModel();
    }

    @Override
    protected void onViewCreated() {
        if (getView() != null) {
            getView().showProduct(mData);
        }
    }

    @Override
    protected void onFirstViewCreated() {
        updateProduct();
    }

    private void updateProduct() {
        mModel.updateProduct(mData);
        if (getView() != null) {
            getView().updateCount(String.valueOf(mData.getCount()));
            getView().updatePrice(String.valueOf(mData.getCount() * mData.getPrice()));
        }
    }
}
