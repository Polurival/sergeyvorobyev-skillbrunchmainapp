package com.softdesign.skillbrunchmainapp.mvp.presenters.impl;

import com.softdesign.skillbrunchmainapp.data.storage.dto.ProductDto;
import com.softdesign.skillbrunchmainapp.data_binding.view_models.CatalogViewModel;
import com.softdesign.skillbrunchmainapp.mvp.models.CatalogModel;
import com.softdesign.skillbrunchmainapp.mvp.models.impl.CatalogModelImpl;
import com.softdesign.skillbrunchmainapp.mvp.presenters.CatalogPresenter;
import com.softdesign.skillbrunchmainapp.mvp.views.CatalogView;

import java.util.List;

/**
 * @author Sergey Vorobyev.
 */

public class CatalogPresenterImpl extends AbsPresenter<CatalogView, CatalogViewModel>
        implements CatalogPresenter {

    private static final CatalogPresenter instance = new CatalogPresenterImpl();

    private CatalogModel mModel;

    private List<ProductDto> mData;

    private int mProductCounter;

    private CatalogPresenterImpl() {
        mModel = new CatalogModelImpl();
    }

    public static CatalogPresenter getInstance() {
        return instance;
    }

    @Override
    public void OnBuyButtonClick(int position) {
        if (getView() != null) {
            if (!mModel.checkAuthorisation()) {
                getView().navigateToAuth();
            } else {
                ProductDto product = mData.get(position);
                mProductCounter += product.getCount();
                getView().updateProductCounter(String.valueOf(mProductCounter));
                getView().showMessageAddToCard(product.getProductName());
            }
        }
    }

    @Override
    protected CatalogViewModel createViewModel() {
        return new CatalogViewModel();
    }

    @Override
    protected void onViewCreated() {
        if (getView() != null) {
            getView().showCatalog(mData);
            getView().updateProductCounter(String.valueOf(mProductCounter));
        }
    }

    @Override
    protected void onFirstViewCreated() {
        mData = mModel.getProductList();
    }
}
