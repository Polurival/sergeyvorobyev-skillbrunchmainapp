package com.softdesign.skillbrunchmainapp.mvp.models.impl;

import com.softdesign.skillbrunchmainapp.data.managers.DataManager;
import com.softdesign.skillbrunchmainapp.mvp.models.AuthModel;

import java.util.Random;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * @author Sergey Vorobyev.
 */

public class AuthModelImpl implements AuthModel {

    private Random mRandom;
    private DataManager mDataManager;

    public AuthModelImpl() {
        mRandom = new Random();
        mDataManager = DataManager.getInstance();
    }

    @Override
    public boolean checkAuthorisation() {
        return mDataManager.checkAuthorisation();
    }

    @Override
    public Observable<Boolean> login(String email, String password) {
        return Observable.just(mRandom.nextBoolean())
                .delay(3000, TimeUnit.MILLISECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public void logout() {
        mDataManager.deleteToken();
    }

    @Override
    public void saveToken(String token) {
        mDataManager.saveToken(token);
    }
}
