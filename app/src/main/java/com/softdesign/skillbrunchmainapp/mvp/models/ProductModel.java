package com.softdesign.skillbrunchmainapp.mvp.models;

import com.softdesign.skillbrunchmainapp.data.storage.dto.ProductDto;

/**
 * @author Sergey Vorobyev.
 */

public interface ProductModel extends Model {

    void updateProduct(ProductDto product);
}
