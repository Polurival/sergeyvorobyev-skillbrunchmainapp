package com.softdesign.skillbrunchmainapp.mvp.presenters.impl;

import com.softdesign.skillbrunchmainapp.data_binding.view_models.MainViewModel;
import com.softdesign.skillbrunchmainapp.mvp.presenters.MainPresenter;
import com.softdesign.skillbrunchmainapp.mvp.views.MainView;

/**
 * @author Sergey Vorobyev.
 */

public class MainPresenterImpl extends AbsPresenter<MainView, MainViewModel>
        implements MainPresenter {

    private static MainPresenter instance = new MainPresenterImpl();

    private MainPresenterImpl() {
    }

    public static MainPresenter getInstance() {
        return instance;
    }

    @Override
    public void onNavigationItemAccountClick() {
        if (getView() != null) {
            getView().navigateToAccount();
            getView().closeDrawer();
        }
    }

    @Override
    public void onNavigationItemCatalogClick() {
        if (getView() != null) {
            getView().navigateToCatalog();
            getView().closeDrawer();
        }
    }

    @Override
    public void onNavigationItemFavoritesClick() {
        if (getView() != null) {
            getView().navigateToFavorites();
            getView().closeDrawer();
        }
    }

    @Override
    public void onNavigationItemOrdersClick() {
        if (getView() != null) {
            getView().navigateToOrders();
            getView().closeDrawer();
        }
    }

    @Override
    public void onNavigationItemNotificationsClick() {
        if (getView() != null) {
            getView().navigateToNotifications();
            getView().closeDrawer();
        }
    }

    @Override
    public void onDrawerToggleClick() {
        if (getView() != null) {
            getView().openDrawer();
        }
    }

    @Override
    public void onSystemBackButtonClick() {
        if (getView() != null) {
            if (getView().isDrawerOpened()) {
                getView().closeDrawer();
            } else if (!getView().isBackStackIsEmpty()) {
                getView().pressSystemBackButton();
            } else {
                getView().showExitConfirmDialog();
            }
        }
    }

    @Override
    public void onPositiveDialogButtonClick() {
        if (getView() != null) {
            getView().exit();
        }
    }

    @Override
    public void onNegativeDialogButtonClick() {

    }

    @Override
    protected MainViewModel createViewModel() {
        return new MainViewModel();
    }

    @Override
    protected void onViewCreated() {

    }

    @Override
    protected void onFirstViewCreated() {
        if (getView() != null) {
            getView().setAvatar("file:///android_asset/user_avatar.jpg");
            getView().setUserName("User first name");
        }
    }
}
