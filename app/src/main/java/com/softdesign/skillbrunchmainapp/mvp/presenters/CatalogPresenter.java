package com.softdesign.skillbrunchmainapp.mvp.presenters;

/**
 * @author Sergey Vorobyev.
 */

public interface CatalogPresenter extends Presenter {

    void OnBuyButtonClick(int position);
}
