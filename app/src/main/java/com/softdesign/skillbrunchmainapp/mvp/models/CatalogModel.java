package com.softdesign.skillbrunchmainapp.mvp.models;

import com.softdesign.skillbrunchmainapp.data.storage.dto.ProductDto;

import java.util.List;

/**
 * @author Sergey Vorobyev.
 */

public interface CatalogModel extends Model {

    List<ProductDto> getProductList();

    ProductDto getProductById(long id);

    boolean checkAuthorisation();
}
