package com.softdesign.skillbrunchmainapp.data_binding.view_models;

import android.databinding.Bindable;

import com.softdesign.skillbrunchmainapp.BR;

/**
 * @author Sergey Vorobyev.
 */

public class MainViewModel extends AbsViewModel {

    private DrawerHeaderData drawerHeaderData;

    public MainViewModel() {
        this.drawerHeaderData = new DrawerHeaderData();
    }

    @Bindable
    public DrawerHeaderData getDrawerHeaderData() {
        return drawerHeaderData;
    }

    public void setDrawerHeaderAvatar(String avatarUri) {
        drawerHeaderData.setAvatarUri(avatarUri);
        notifyPropertyChanged(BR.drawerHeaderData);
    }

    public void setDrawerHeaderUsername(String username) {
        drawerHeaderData.setUsername(username);
        notifyPropertyChanged(BR.drawerHeaderData);
    }
}
