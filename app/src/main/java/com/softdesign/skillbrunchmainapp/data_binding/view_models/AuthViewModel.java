package com.softdesign.skillbrunchmainapp.data_binding.view_models;

import android.databinding.Bindable;

import com.softdesign.skillbrunchmainapp.BR;

/**
 * @author Sergey Vorobyev.
 */

public class AuthViewModel extends AbsViewModel {

    private boolean isShowingCatalogButton;
    private boolean isShowingSocialButtons;
    private boolean isShowingProgress;
    private boolean isShowingInputs;
    private boolean isLoginButtonEnabled;

    private boolean isPasswordValid;
    private boolean isEmailValid;

    private String loginButtonText;
    private String password;
    private String email;

    @Bindable
    public boolean isShowingCatalogButton() {
        return isShowingCatalogButton;
    }

    public void setShowingCatalogButton(boolean showingCatalogButton) {
        isShowingCatalogButton = showingCatalogButton;
        notifyPropertyChanged(BR.showingCatalogButton);
    }

    @Bindable
    public boolean isShowingSocialButtons() {
        return isShowingSocialButtons;
    }

    public void setShowingSocialButtons(boolean showingSocialButtons) {
        isShowingSocialButtons = showingSocialButtons;
        notifyPropertyChanged(BR.showingSocialButtons);
    }

    @Bindable
    public boolean isShowingProgress() {
        return isShowingProgress;
    }

    public void setShowingProgress(boolean showingProgress) {
        isShowingProgress = showingProgress;
        notifyPropertyChanged(BR.showingProgress);
    }

    @Bindable
    public boolean isShowingInputs() {
        return isShowingInputs;
    }

    public void setShowingInputs(boolean showingInputs) {
        isShowingInputs = showingInputs;
        notifyPropertyChanged(BR.showingInputs);
    }

    @Bindable
    public boolean isLoginButtonEnabled() {
        return isLoginButtonEnabled;
    }

    public void setLoginButtonEnabled(boolean loginButtonEnabled) {
        isLoginButtonEnabled = loginButtonEnabled;
        notifyPropertyChanged(BR.loginButtonEnabled);
    }

    @Bindable
    public boolean isPasswordValid() {
        return isPasswordValid;
    }

    public void setPasswordValid(boolean passwordValid) {
        isPasswordValid = passwordValid;
        notifyPropertyChanged(BR.passwordValid);
    }

    @Bindable
    public boolean isEmailValid() {
        return isEmailValid;
    }

    public void setEmailValid(boolean emailValid) {
        isEmailValid = emailValid;
        notifyPropertyChanged(BR.emailValid);
    }

    @Bindable
    public String getLoginButtonText() {
        return loginButtonText;
    }

    public void setLoginButtonText(String loginButtonText) {
        this.loginButtonText = loginButtonText;
        notifyPropertyChanged(BR.loginButtonText);
    }

    @Bindable
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
        notifyPropertyChanged(BR.password);
    }

    @Bindable
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
        notifyPropertyChanged(BR.email);
    }
}
