package com.softdesign.skillbrunchmainapp.data_binding;

import android.databinding.BindingAdapter;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TextInputLayout;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.softdesign.skillbrunchmainapp.R;
import com.softdesign.skillbrunchmainapp.data.managers.DataManager;
import com.softdesign.skillbrunchmainapp.data_binding.view_models.DrawerHeaderData;
import com.softdesign.skillbrunchmainapp.ui.custom_views.AspectRatioImageView;
import com.softdesign.skillbrunchmainapp.utils.CircleTransformation;
import com.softdesign.skillbrunchmainapp.utils.Constants;
import com.squareup.picasso.Callback;

import static com.softdesign.skillbrunchmainapp.BuildConfig.DEBUG;
import static com.softdesign.skillbrunchmainapp.ui.animations.AnimationUtils.animateAlphaVisibleGone;
import static com.softdesign.skillbrunchmainapp.ui.animations.AnimationUtils.animateAlphaVisibleInvisible;
import static com.squareup.picasso.NetworkPolicy.OFFLINE;

/**
 * @author Sergey Vorobyev.
 */

public class BindingUtils {

    private static final String TAG = Constants.LOG_TAG_PREFIX + "BindingUtils";

    @BindingAdapter("hasErrorEmail")
    public static void bindErrorEmail(TextInputLayout view, Boolean isError) {
        view.setErrorEnabled(isError);
        view.setError(isError ?
                view.getResources().getString(R.string.error_edit_text_wrong_format) : null);
    }

    @BindingAdapter("hasErrorPassword")
    public static void bindErrorPassword(TextInputLayout v, Boolean isError) {
        v.setErrorEnabled(isError);
        v.setError(isError ?
                v.getResources().getString(R.string.error_edit_text_wrong_length) : null);
    }

    @BindingAdapter("animateAlphaGoneOrVisible")
    public static void animateAlphaGoneOrVisible(View view, boolean isVisible) {
        animateAlphaVisibleGone(view, isVisible);
    }

    @BindingAdapter("animateAlphaInvisibleOrVisible")
    public static void animateAlphaInvisibleOrVisible(View view, boolean isVisible) {
        animateAlphaVisibleInvisible(view, isVisible);
    }

    @BindingAdapter("srcUriAspectRatioImageView")
    public static void srcUriAspectRatioImageView(AspectRatioImageView view, String uri) {
        int width = view.getContext().getResources().getDisplayMetrics().widthPixels;
        int height = (int) (width / view.getAspectRatio());
        DataManager.getInstance().getPicasso()
                .load(uri)
                .placeholder(R.color.color_gray_light)
                .resize(width, height)
                .onlyScaleDown()
                .centerCrop()
                .networkPolicy(OFFLINE)
                .into(view, new Callback() {
                    @Override
                    public void onSuccess() {
                        if (DEBUG) {
                            Log.d(TAG, "aspect ratio image loaded from cache");
                        }
                    }

                    @Override
                    public void onError() {
                        DataManager.getInstance().getPicasso()
                                .load(uri)
                                .placeholder(R.color.color_gray_light)
                                .resize(width, height)
                                .onlyScaleDown()
                                .centerCrop()
                                .into(view, new Callback() {
                                    @Override
                                    public void onSuccess() {
                                        if (DEBUG) {
                                            Log.d(TAG, "aspect ratio image loaded from server");
                                        }
                                    }

                                    @Override
                                    public void onError() {
                                        if (DEBUG) {
                                            Log.d(TAG, "Can't load aspect ratio image from server");
                                        }
                                    }
                                });
                    }
                });
    }

    @BindingAdapter("drawerHeaderData")
    public static void drawerHeaderData(NavigationView view, DrawerHeaderData data) {
        ImageView avatar = (ImageView) view.getHeaderView(0).findViewById(R.id.drawer_avatar);
        TextView userName = (TextView) view.getHeaderView(0).findViewById(R.id.drawer_username);
        userName.setText(data.getUsername());

        DataManager.getInstance().getPicasso()
                .load(data.getAvatarUri())
                .placeholder(R.color.color_gray_light)
                .resizeDimen(R.dimen.drawer_header_avatar_size, R.dimen.drawer_header_avatar_size)
                .onlyScaleDown()
                .centerCrop()
                .transform(new CircleTransformation())
                .networkPolicy(OFFLINE)
                .into(avatar, new Callback() {
                    @Override
                    public void onSuccess() {
                        if (DEBUG) {
                            Log.d(TAG, "avatar loaded from cache");
                        }
                    }

                    @Override
                    public void onError() {
                        DataManager.getInstance().getPicasso()
                                .load(data.getAvatarUri())
                                .placeholder(R.color.color_gray_light)
                                .resizeDimen(R.dimen.drawer_header_avatar_size,
                                        R.dimen.drawer_header_avatar_size)
                                .onlyScaleDown()
                                .centerCrop()
                                .transform(new CircleTransformation())
                                .into(avatar, new Callback() {
                                    @Override
                                    public void onSuccess() {
                                        if (DEBUG) {
                                            Log.d(TAG, "avatar loaded from server");
                                        }
                                    }

                                    @Override
                                    public void onError() {
                                        if (DEBUG) {
                                            Log.d(TAG, "Can't load avatar from server");
                                        }
                                    }
                                });
                    }
                });
    }
}
