package com.softdesign.skillbrunchmainapp.data_binding.view_models;

/**
 * @author Sergey Vorobyev.
 */

public class DrawerHeaderData {

    private String avatarUri;

    private String username;

    public String getAvatarUri() {
        return avatarUri;
    }

    public void setAvatarUri(String avatarUri) {
        this.avatarUri = avatarUri;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
