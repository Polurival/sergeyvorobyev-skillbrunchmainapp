package com.softdesign.skillbrunchmainapp.data_binding.view_models;

import android.databinding.BaseObservable;

/**
 * @author Sergey Vorobyev.
 */

public abstract class AbsViewModel extends BaseObservable {
}
